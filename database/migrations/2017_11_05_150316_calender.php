<?php

use Illuminate\Database\Schema\Blueprint;
use Anomaly\Streams\Platform\Database\Migration\Migration;

class Calender extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calendar', function (Blueprint $table) {
            $table->increments('id')->nullable();
            $table->string('date')->nullable();
            $table->string('place')->nullable();
            $table->string('sector')->nullable();
            $table->string('nation')->nullable();
            $table->string('discipline')->nullable();
            $table->string('category')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('calendar')) {
            Schema::table('calendar', function (Blueprint $collection) {
                $collection->drop();
            });
        }
    }
}
