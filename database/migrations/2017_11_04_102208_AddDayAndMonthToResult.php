<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class AddDayAndMonthToResult extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('results', function($table) {
            $table->string('month');
            $table->string('day');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('results', function($table) {
            $table->dropColumn('month');
            $table->dropColumn('day');
        });
    }
}
