<?php

use Illuminate\Database\Schema\Blueprint;
use Anomaly\Streams\Platform\Database\Migration\Migration;

class Results extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


            Schema::create('results', function (Blueprint $table) {
                $table->increments('id')->nullable();
                $table->string('date')->nullable();
                $table->string('location')->nullable();
                $table->string('country')->nullable();
                $table->string('race')->nullable();
                $table->string('discipline')->nullable();
                $table->string('place')->nullable();
                $table->string('points')->nullable();
                $table->timestamps();
            });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('results')) {
            Schema::table('results', function (Blueprint $collection) {
                $collection->drop();
            });
        }
    }
}
