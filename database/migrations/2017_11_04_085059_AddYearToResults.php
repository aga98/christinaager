<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class AddYearToResults extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('results', function($table) {
            $table->string('year');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('results', function($table) {
            $table->dropColumn('year');
        });
    }
}
