<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

class GetCalendar extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'GetCalendar:fetch';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Goes on FIS Website and fetch the calendar';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        echo "Start Import";

        echo "....";

        $htmlContent = file_get_contents("https://data.fis-ski.com/global-links/calendar.html?place_search=&seasoncode_search=2019&sector_search=AL&date_search=&gender_search=l&category_search=WC&codex_search=&nation_search=&disciplinecode_search=SG%2C+DH%2C+AC&date_from=today&search=Search&limit=50");
        $i = 0;
        $counter = 2;

        $DOM = new \DOMDocument();
        libxml_use_internal_errors(true);
        $DOM->loadHTML($htmlContent);

        $Header = $DOM->getElementsByTagName('td');

        foreach($Header as $NodeHeader)
        {
            $aDataTableHeaderHTML[] = trim($NodeHeader->textContent);
        }

        $arraysize = count($aDataTableHeaderHTML);
        $arraysize = $arraysize - 1;

        while ($i <= 1)
        {
            unset($aDataTableHeaderHTML[$i]);
            $i++;
        }

        while ($counter <= $arraysize)
        {
            $date = $aDataTableHeaderHTML[$counter];
            $counter++;
            $location = $aDataTableHeaderHTML[$counter];
            $counter++;
            $sector = $aDataTableHeaderHTML[$counter];
            $counter++;
            $nation = $aDataTableHeaderHTML[$counter];
            $counter++;
            $counter++;
            $discipline = $aDataTableHeaderHTML[$counter];
            $counter++;
            $counter++;
            $category = $aDataTableHeaderHTML[$counter];
            $counter++;
            $counter++;
            $counter++;
            $counter++;
            $counter++;
            $counter++;

            $ResultCount = DB::table('calendar')
                ->where('date', '=', $date)
                ->count();

            if($ResultCount == 0)
            {
                DB::table('calendar')->insert(
                    [
                        'date' => $date,
                        'place' => $location,
                        'sector' => $sector,
                        'nation' => $nation,
                        'discipline' => $discipline,
                        'category' => $category,
                    ]
                );
            }
        }

        echo "END Import";

        echo "....";
    }
}
