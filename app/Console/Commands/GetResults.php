<?php

namespace App\Console\Commands;

use DB;
use Carbon\Carbon;
use Illuminate\Console\Command;

class GetResults extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:results';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Goes on FIS Website and fetch the results';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->output = new \Symfony\Component\Console\Output\ConsoleOutput();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->output->writeln('Importing Results...');

        $htmlContent = file_get_contents("https://data.fis-ski.com/fis_athletes/ajax/athletesfunctions/load_results.html?sectorcode=AL&competitorid=166907&categorycode=&disciplinecode=&sort=&seasoncode=&position=&place=&limit=200");
        $clearedResults = [];

        $DOM = new \DOMDocument();
        libxml_use_internal_errors(true);
        $DOM->loadHTML($htmlContent);

        $resultsMarkup = $DOM->getElementsByTagName('a');

        foreach($resultsMarkup as $resultMarkup)
        {
            $results[] = trim($resultMarkup->textContent);
        }

        foreach($results as $result)
        {
            $explodedResult = explode("\n", $result);

            $clearedResults[] = [
                'date' => $explodedResult[0],
                'location' => trim($explodedResult[1]),
                'country' => trim($explodedResult[12]),
                'race' => isset($explodedResult[18]) ? trim($explodedResult[18]) : null,
                'fis_points' => isset($explodedResult[23]) ? trim($explodedResult[23]) : null,
                'place' => isset($explodedResult[21]) ? trim($explodedResult[21]) : null,
                'discipline' => trim($explodedResult[4]),
            ];
        }

        foreach($clearedResults as $result) {
            try {
                Carbon::setLocale('de');
                setlocale(LC_TIME, 'German');
                $date = Carbon::createFromFormat('d-m-Y', $result['date']);

                $countResults = DB::table('results')
                    ->where('points', '=', $result['fis_points'])
                    ->where('date', '=', $date->format('d-m-Y'))
                    ->count();

                if($countResults == 0) {
                    DB::table('results')->insert(
                        [
                            'date' => $date->format('d-m-Y'),
                            'location' => $result['location'],
                            'country' => $result['country'],
                            'race' => $result['race'],
                            'discipline' => $result['discipline'],
                            'place' => $result['place'],
                            'points' => $result['fis_points'],
                            'year' => $date->format('Y'),
                            'month' => $date->format('F'),
                            'day' => $date->format('d'),
                        ]
                    );

                    $this->output->writeln("<comment>importing -> </comment>".$result['date'] . " " . $result['location'] . " " . $result['country'] . " " . $result['discipline']. " " . $result['fis_points']. " " . $result['place']);
                }
            } catch (\Exception $e) {
                $this->output->writeln('<error>ERROR</error>');
            }

        }

        $this->output->writeln('<info>done</info>');
    }
}
