<?php namespace Christinaager\InstagramPlugin;

use Anomaly\Streams\Platform\Addon\Plugin\Plugin;
use Twig_Environment;
use Twig_NodeVisitorInterface;
use Vinkla\Instagram\Instagram;
use DB;

class InstagramPlugin extends Plugin
{

    /**
     * Returns a list of functions to add to the existing list.
     *
     * @return array An array of functions
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('InstagramFeed', function() {

                try {
                // use this instagram access token generator http://instagram.pixelunion.net/
                $access_token="4956515.1677ed0.36ce6b2714fb4787b4577212185b098c";
                $photo_count=12;

                $json_link="https://api.instagram.com/v1/users/4956515/media/recent/?access_token={$access_token}&count={$photo_count}";

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_URL,$json_link);
                $InstagramFeed=curl_exec($ch);
                curl_close($ch);

                $InstagramFeed = json_decode($InstagramFeed);
                $InstagramFeed = $InstagramFeed->data;

                //return view('theme::partials/instagram', ['InstagramFeed' => 'InstagramFeed']);


                    foreach($InstagramFeed as $post) {
                        //var_dump($post);
                        echo "<div class='instagram-post wow zoomIn'>";
                        echo "<div class='instagram-post-img'>";
                        echo "<i class='fa fa-instagram' aria-hidden='true'></i>";
                        echo "<img src='".$post->images->standard_resolution->url."' class='img-responsive'>";
                        echo "</div>";
                        echo "<div class='instagram-post-text'>";
                        echo $post->caption->text;
                        echo "</div>";
                        echo "</div>";
                    }
                } catch (\Exception $e) {

                }
            })
        ];
    }

    /**
     * Returns a list of global variables to add to the existing list.
     *
     * @return array An array of global variables
     */
    public function getGlobals()
    {
        return [];
    }

    /**
     * Returns a list of filters to add to the existing list.
     *
     * @return array An array of filters
     */
    public function getFilters()
    {
        return [];
    }

    /**
     * Initializes the runtime environment.
     *
     * This is where you can load some file that contains filter functions for instance.
     *
     * @param Twig_Environment $environment The current Twig_Environment instance
     */
    public function initRuntime(Twig_Environment $environment)
    {
    }

    /**
     * Returns the token parser instances to add to the existing list.
     *
     * @return array An array of Twig_TokenParserInterface or Twig_TokenParserBrokerInterface instances
     */
    public function getTokenParsers()
    {
        return [];
    }

    /**
     * Returns the node visitor instances to add to the existing list.
     *
     * @return Twig_NodeVisitorInterface[] An array of Twig_NodeVisitorInterface instances
     */
    public function getNodeVisitors()
    {
        return [];
    }

    /**
     * Returns a list of tests to add to the existing list.
     *
     * @return array An array of tests
     */
    public function getTests()
    {
        return [];
    }

    /**
     * Returns a list of operators to add to the existing list.
     *
     * @return array An array of operators
     */
    public function getOperators()
    {
        return [];
    }

}
