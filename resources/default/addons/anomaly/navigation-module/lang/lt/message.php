<?php

return [
    'broken' => 'Sugadintas',
    'restricted' => 'Ribotas',
    'external' => 'Linkas į iššorę',
    'choose_link_type' => 'Kokio tipo linką norite sukurti?',
    'choose_menu' => 'Kurio meniu linkus norėtumėte valdyti?',
];
