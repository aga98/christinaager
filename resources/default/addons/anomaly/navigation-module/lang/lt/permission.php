<?php

return [
    'menus' => [
        'name' => 'Meniu',
        'option' => [
            'read' => 'Prieiga prie meniu skyriaus.',
            'write' => 'Gali kurti ir redaguoti meniu.',
            'delete' => 'Gali ištrinti meniu.',
        ],
    ],
    'links' => [
        'name' => 'Linkai',
        'option' => [
            'read' => 'Prieiga prie linkų skyriaus',
            'write' => 'Gali sukurti ir redaguoti linkus.',
            'delete' => 'Gali ištrinti linkus.',
        ],
    ],
];
