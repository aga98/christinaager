<?php

return [
    'title' => 'Navigacijos',
    'name' => 'Navigacijų modulis',
    'description' => 'Navigacijų valdymas paprastai.',
    'section' => [
        'menus' => 'Meniu',
        'links' => 'Linkai',
    ],
];
